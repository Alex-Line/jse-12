package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.Collection;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        if (session.getRole() != Role.ADMINISTRATOR) {
            @NotNull final Collection<Project> collection =
                    projectEndpoint.removeAllProjectsByUserId(session, session.getUserId());
            if (collection.isEmpty()) return;
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        } else {
            @NotNull final Collection<Project> collection =
                    projectEndpoint.removeAllProjects(session);
            if (collection.isEmpty()) return;
            System.out.println("[All PROJECTS REMOVED]\n");
        }

        System.out.println("[THERE IS NOT ANY PROJECTS FOR REMOVING]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}