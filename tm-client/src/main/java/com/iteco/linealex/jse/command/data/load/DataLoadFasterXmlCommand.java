package com.iteco.linealex.jse.command.data.load;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataLoadFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY FASTER_XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM XML FILE");
        serviceLocator.getUserEndpoint().loadFasterXml(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}