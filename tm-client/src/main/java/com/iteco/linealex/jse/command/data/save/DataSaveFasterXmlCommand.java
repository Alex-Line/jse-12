package com.iteco.linealex.jse.command.data.save;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataSaveFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO XML FILE BY FASTER-XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO XML FILE");
        serviceLocator.getUserEndpoint().saveFasterXml(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}