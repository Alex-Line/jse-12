package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.Collection;
import java.util.Collections;

public class TaskListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list-status";
    }

    @Override
    public String description() {
        return "LIST TASKS SORTED BY STATUS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final User selectedUser = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Project selectedProject = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<Task> collection = Collections.EMPTY_LIST;
        if (session.getRole() == Role.ADMINISTRATOR)
            collection = serviceLocator.getTaskEndpoint().getAllTasksSortedByStatus(session);
        else if (selectedProject != null)
            collection = serviceLocator.getTaskEndpoint()
                    .getAllTasksSortedByStatusWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId());
        else collection = serviceLocator.getTaskEndpoint()
                    .getAllTasksSortedByStatusWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}