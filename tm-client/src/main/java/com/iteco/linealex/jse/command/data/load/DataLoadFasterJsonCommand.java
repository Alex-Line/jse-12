package com.iteco.linealex.jse.command.data.load;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataLoadFasterJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-faster-json";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM JSON FILE BY FASTER_XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM JSON FILE");
        serviceLocator.getUserEndpoint().loadFasterJson(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}