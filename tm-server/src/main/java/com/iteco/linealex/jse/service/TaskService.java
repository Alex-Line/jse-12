package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    public TaskService(
            @NotNull final IPropertyService propertyService
    ) {
        super(propertyService);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAll(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAll(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Task getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findOneByName(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Task persist(
            @Nullable final Task entity
    ) throws Exception {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).persist(entity);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> persist(
            @NotNull final Collection<Task> collection
    ) throws Exception {
        if (collection.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).persist(collection);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Task merge(
            @Nullable final Task entity
    ) throws Exception {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).merge(entity);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String entityName
    ) throws Exception {
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findOneByName(entityName);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findOneByName(userId, entityName);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllByName(userId, pattern);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllByName(pattern);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStartDate();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStartDate(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByFinishDate();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByFinishDate(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStatus();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStatus(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Task removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).remove(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> removeAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).removeAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<Task> removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return this.removeAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).removeAll(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return false;
        if (selectedEntity == null) return false;
        @NotNull final Connection connection = configureConnection();
        try {
            selectedEntity.setProjectId(projectId);
            @Nullable final Task task = new TaskRepository(connection).merge(selectedEntity);
            if (task == null) return false;
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return true;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findOneByName(userId, projectId, taskName);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllByName(userId, projectId, pattern);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStartDate(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByFinishDate(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new TaskRepository(connection).findAllSortedByStatus(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

}