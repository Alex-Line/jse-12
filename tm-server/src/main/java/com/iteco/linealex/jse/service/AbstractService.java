package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IService;
import com.iteco.linealex.jse.entity.AbstractEntity;
import com.iteco.linealex.jse.util.JDBCConnectorUtils;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Collection;

@Getter
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IPropertyService propertyService;

    public AbstractService(
            @NotNull final IPropertyService propertyService
    ) {
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public abstract T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities() throws Exception;

    @Nullable
    @Override
    public abstract T persist(
            @Nullable final T entity
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> persist(
            @NotNull final Collection<T> collection
    ) throws Exception;

    @Nullable
    @Override
    public abstract T removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> removeAllEntities() throws Exception;

    @NotNull
    public Connection configureConnection() throws Exception {
        @Nullable final String url = propertyService.getProperty("DB_URL");
        @Nullable final String user = propertyService.getProperty("DB_USER");
        @Nullable final String password = propertyService.getProperty("DB_PASSWORD");
        return JDBCConnectorUtils.getConnection(url, user, password);
    }

}