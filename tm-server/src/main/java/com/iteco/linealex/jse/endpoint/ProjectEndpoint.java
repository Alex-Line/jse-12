package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IProjectService projectService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionService, propertyService);
        this.projectService = projectService;
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        return projectService.getEntityById(entityId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntities();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntities(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> persistProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects") @NotNull final Collection<Project> collection
    ) throws Exception {
        validateSession(session);
        return projectService.persist(collection);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        return projectService.removeEntity(entityId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return projectService.removeAllEntities(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> removeAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return projectService.removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public Project persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws Exception {
        validateSession(session);
        return projectService.persist(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Project mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws Exception {
        validateSession(session);
        return projectService.merge(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        return projectService.getEntityByName(entityName);
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        return projectService.getEntityByName(userId, entityName);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesByName(userId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesByName(pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByStartDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByStartDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByFinishDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByStatus();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return projectService.getAllEntitiesSortedByStatus(userId);
    }

}