package com.iteco.linealex.jse.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    @Nullable
    public T persist(
            @Nullable final T entity
    ) throws Exception;

    @NotNull
    public Collection<T> persist(
            @NotNull final Collection<T> collection
    ) throws Exception;

    @Nullable
    public T merge(
            @Nullable final T entity
    ) throws Exception;

    @Nullable
    public T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    public Collection<T> getAllEntities() throws Exception;

    @Nullable
    public T removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    public Collection<T> removeAllEntities() throws Exception;

}