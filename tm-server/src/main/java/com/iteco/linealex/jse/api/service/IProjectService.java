package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    @NotNull
    public Collection<Project> getAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @Nullable
    public Project getEntityByName(
            @Nullable final String entityName
    ) throws Exception;

    @Nullable
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception;

    @NotNull
    public Collection<Project> removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

}