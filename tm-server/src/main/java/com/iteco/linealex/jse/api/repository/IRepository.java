package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(@NotNull final String entityId) throws SQLException;

    @NotNull
    public Collection<T> findAll() throws SQLException;

    @Nullable
    public T persist(@NotNull final T example) throws InsertExistingEntityException, NoSuchAlgorithmException, SQLException;

    @NotNull
    public Collection<T> persist(@NotNull final Collection<T> collection) throws SQLException;

    @Nullable
    public T merge(@NotNull final T example) throws InsertExistingEntityException, SQLException;

    @Nullable
    public T remove(@NotNull final String name) throws SQLException;

    @NotNull
    public Collection<T> removeAll() throws SQLException;

}