package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import com.iteco.linealex.jse.exception.user.*;
import com.iteco.linealex.jse.repository.UserRepository;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final IPropertyService propertyService) {
        super(propertyService);
    }

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) throws Exception {
        if (entityId == null) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).findOne(entityId);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).findAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @NotNull final Connection connection = configureConnection();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        try {
            @Nullable final User user = new UserRepository(connection).findOneByName(login, hashPassword);
            if (user != null && user.getHashPassword() != null
                    && !user.getHashPassword().equals(hashPassword)) throw new WrongPasswordException();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public User createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return null;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            if (new UserRepository(connection).contains(user.getId())) throw new InsertExistingEntityException();
            return persist(user);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public User persist(
            @Nullable final User entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getLogin().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).persist(entity);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<User> persist(
            @NotNull final Collection<User> collection
    ) throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).persist(collection);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public User removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).remove(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<User> removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<User> removeAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).removeAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public User merge(
            @Nullable final User entity
    ) throws Exception {
        if (entity == null) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            @Nullable final User user = new UserRepository(connection).findOne(entity.getId());
            if (user == null) return null;
            user.setLogin(entity.getLogin());
            user.setRole(entity.getRole());
            user.setHashPassword(entity.getHashPassword());
            return user;
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    public User updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        if (selectedUser.getHashPassword() != null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        selectedUser.setHashPassword(hashNewPassword);
        @NotNull final Connection connection = configureConnection();
        try {
            new UserRepository(connection).merge(selectedUser);
            return selectedUser;
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @NotNull final Connection connection = configureConnection();
        try {
            @Nullable final User user = new UserRepository(connection).findOne(login);
            if (user == null) throw new UserIsNotExistException();
            return user;
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        @NotNull final Connection connection = configureConnection();
        try {
            return new UserRepository(connection).findAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

}