package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.entity.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public abstract class AbstractTMService<T extends AbstractTMEntity> extends AbstractService<T> {

    public AbstractTMService(
            @NotNull final IPropertyService propertyService
    ) {
        super(propertyService);
    }

    @Nullable
    @Override
    public abstract T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities() throws Exception;

    @Nullable
    @Override
    public abstract T persist(@Nullable T entity) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> persist(
            @NotNull final Collection<T> collection
    ) throws Exception;

    @Nullable
    @Override
    public abstract T removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> removeAllEntities() throws Exception;

    @Nullable
    @Override
    public abstract T merge(
            @Nullable final T entity
    ) throws Exception;

    @Nullable
    public abstract T getEntityByName(@Nullable final String entityName) throws Exception;

    @Nullable
    public abstract T getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntities(
            final @Nullable String userId
    ) throws Exception;

}