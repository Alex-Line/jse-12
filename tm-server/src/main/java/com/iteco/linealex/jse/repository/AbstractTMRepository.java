package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public abstract class AbstractTMRepository<T extends AbstractTMEntity> extends AbstractRepository<T> {

    public AbstractTMRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    abstract protected T operate(@Nullable final ResultSet row) throws SQLException;

    @Nullable
    public abstract T findOneByName(@NotNull final String entityName) throws SQLException;

    @Nullable
    public abstract T findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) throws SQLException;

    @Nullable
    public abstract T findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String entityName) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllByName(
            @NotNull final String pattern
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStartDate(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStartDate() throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByFinishDate(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByFinishDate() throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStatus(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStatus() throws SQLException;

    public abstract boolean contains(
            @NotNull final String name,
            @NotNull final String userId
    ) throws SQLException;

}