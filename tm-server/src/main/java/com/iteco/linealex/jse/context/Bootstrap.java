package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.endpoint.*;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.service.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;

@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectService projectService = new ProjectService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService, propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService, propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, userService, propertyService, projectService, taskService);

    @NotNull
    final IPropertyEndpoint propertyEndpoint = new PropertyEndpoint(sessionService, propertyService);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService, propertyService);

    public void start() {
        createInitialUsers();
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/propertyService?wsdl", propertyEndpoint);
    }

    private void createInitialUsers() {
        try {
            @NotNull final User admin = new User();
            admin.setLogin(propertyEndpoint.getProperty("ADMIN"));
            admin.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("ADMIN_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            admin.setRole(Role.ADMINISTRATOR);
            userEndpoint.persistUser(admin);
            @NotNull final User user = new User();
            user.setLogin(propertyEndpoint.getProperty("USER"));
            user.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("USER_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            userEndpoint.persistUser(user);
        } catch (TaskManagerException e) {
            System.out.println(e.getMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}