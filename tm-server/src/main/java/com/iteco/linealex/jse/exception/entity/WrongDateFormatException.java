package com.iteco.linealex.jse.exception.entity;

import com.iteco.linealex.jse.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class WrongDateFormatException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "[WRONG DATE FORMAT! COMMAND WAS INTERRUPTED]\n";
    }

}