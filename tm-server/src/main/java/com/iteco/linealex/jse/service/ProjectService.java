package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    public ProjectService(
            @NotNull final IPropertyService propertyService
    ) {
        super(propertyService);
    }

    @Nullable
    @Override
    public Project getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findOneByName(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Project persist(
            @Nullable final Project project
    ) throws Exception {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).persist(project);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Project> persist(
            @NotNull final Collection<Project> collection
    ) throws Exception {
        if (collection.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).persist(collection);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Project removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).remove(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Project> removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> removeAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).removeAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Project merge(
            @Nullable final Project entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).merge(entity);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String entityName
    ) throws Exception {
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findOneByName(entityName);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findOneByName(userId, entityName);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllByName(userId, pattern);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllByName(pattern);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByStartDate();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByStartDate(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByFinishDate();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByFinishDate(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByStatus();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAllSortedByStatus(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new ProjectRepository(connection).findAll(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

}