package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.repository.SessionRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(
            @NotNull final IPropertyService propertyService
    ) {
        super(propertyService);
    }

    @Nullable
    @Override
    public Session getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).findOne(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Session> getAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).findAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Session persist(
            @Nullable final Session entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return null;
        if (entity.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).persist(entity);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Session> persist(
            @NotNull final Collection<Session> collection
    ) throws Exception {
        if (collection.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).persist(collection);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Session removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).remove(entityId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Session> removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Session> removeAllEntities() throws Exception {
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).removeAll();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public Session merge(
            @Nullable final Session entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return null;
        if (entity.getUserId().isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).merge(entity);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String Id
    ) throws Exception {
        if (Id == null || Id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).findOne(Id);
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Session removeSession(
            @Nullable final String sessionId
    ) throws Exception {
        if (sessionId == null || sessionId.isEmpty()) return null;
        @NotNull final Connection connection = configureConnection();
        try {
            return new SessionRepository(connection).remove(sessionId);
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return null;
    }

}