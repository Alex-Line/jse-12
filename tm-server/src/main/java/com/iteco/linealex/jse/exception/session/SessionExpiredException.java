package com.iteco.linealex.jse.exception.session;

import com.iteco.linealex.jse.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;

public class SessionExpiredException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THIS SESSION EXPIRED. YOU HAVE TO LOG IN AGAIN";
    }

}