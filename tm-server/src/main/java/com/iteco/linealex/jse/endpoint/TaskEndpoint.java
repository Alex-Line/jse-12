package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionService, propertyService);
        this.taskService = taskService;
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        return taskService.getEntityById(entityId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntities();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<Task> collection
    ) throws Exception {
        validateSession(session);
        return taskService.persist(collection);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        return taskService.removeEntity(entityId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.removeAllEntities(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public Task persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception {
        validateSession(session);
        return taskService.persist(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Task mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception {
        validateSession(session);
        return taskService.merge(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        return taskService.getEntityByName(entityName);
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        return taskService.getEntityByName(userId, entityName);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(userId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntities(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntities(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.removeAllTasksFromProject(userId, projectId);
    }

    @Override
    @WebMethod
    public boolean attachTaskToProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "task", partName = "task") @Nullable final Task selectedEntity
    ) throws Exception {
        validateSession(session);
        return taskService.attachTaskToProject(projectId, selectedEntity);
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws Exception {
        validateSession(session);
        return taskService.getEntityByName(userId, projectId, taskName);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(userId, projectId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus(userId, projectId);
    }

}