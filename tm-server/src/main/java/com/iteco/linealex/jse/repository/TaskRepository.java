package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.enumerate.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractTMRepository<Task> implements IRepository<Task> {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    protected Task operate(@Nullable ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("taskId"));
        task.setProjectId(row.getString("projects_projectId"));
        task.setDescription(row.getString("taskDescription"));
        task.setName(row.getString("taskName"));
        task.setDateStart(row.getDate("taskStartDate"));
        task.setDateFinish(row.getDate("taskFinishDate"));
        task.setUserId(row.getString("users_userId"));
        task.setStatus(Status.valueOf(row.getString("taskStatus")));
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE users_userId = ? " +
                "AND projects_projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public Task findOne(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE taskId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.next()) task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> findAll(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE users_userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final String entityName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE taskName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.next()) task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final String userId,
            @NotNull final String taskName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.task " +
                "WHERE users_userId = ? AND taskName = ? AND projects_projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(1, taskName);
        preparedStatement.setString(3, null);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.next()) task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.task " +
                "WHERE users_userId = ? AND projects_projectId = ? AND taskName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, taskName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.next()) task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return task;
    }

    @Nullable
    @Override
    public Task persist(
            @NotNull final Task example
    ) throws SQLException {
        @NotNull final String query = "INSERT INTO TaskManager.tasks " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getId());
        preparedStatement.setString(2, example.getName());
        preparedStatement.setString(3, example.getDescription());
        preparedStatement.setDate(4, new Date(example.getDateStart().getTime()));
        preparedStatement.setDate(5, new java.sql.Date(example.getDateFinish().getTime()));
        preparedStatement.setString(6, example.getStatus().getName().toUpperCase());
        preparedStatement.setString(7, example.getUserId());
        preparedStatement.setString(8, example.getProjectId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @NotNull
    @Override
    public Collection<Task> persist(
            @NotNull final Collection<Task> collection
    ) throws SQLException {
        for (Task example : collection) {
            @NotNull final String query = "INSERT INTO TaskManager.tasks " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, example.getId());
            preparedStatement.setString(2, example.getProjectId());
            preparedStatement.setString(3, example.getDescription());
            preparedStatement.setDate(4, new java.sql.Date(example.getDateFinish().getTime()));
            preparedStatement.setString(5, example.getName());
            preparedStatement.setDate(6, new Date(example.getDateStart().getTime()));
            preparedStatement.setString(7, example.getStatus().getName().toUpperCase());
            preparedStatement.setString(8, example.getUserId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return collection;
    }

    @Nullable
    @Override
    public Task merge(
            @NotNull final Task example
    ) throws SQLException {
        @NotNull final String query = "UPDATE TaskManager.tasks SET " +
                "projects_projectId=?, taskDescription=?, taskFinishDate=?, taskName=?, " +
                "taskStartDate=?, taskStatus=?, users_userId=? where taskId=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getDescription());
        preparedStatement.setDate(2, new Date(example.getDateFinish().getTime()));
        preparedStatement.setString(3, example.getName());
        preparedStatement.setDate(4, new Date(example.getDateStart().getTime()));
        preparedStatement.setString(5, example.getStatus().getName().toUpperCase());
        preparedStatement.setString(6, example.getUserId());
        preparedStatement.setString(7, example.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @Nullable
    @Override
    public Task remove(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE taskId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.next()) task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (task == null) return null;
        @NotNull final String removeQuery = "DELETE FROM TaskManager.task WHERE taskId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, entityId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll() throws SQLException {
        Collection<Task> collection = findAll();
        @NotNull final String removeQuery = "DELETE FROM TaskManager.task;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(
            @NotNull final String userId
    ) throws SQLException {
        Collection<Task> collection = findAll(userId);
        @NotNull final String removeQuery = "DELETE FROM TaskManager.tasks WHERE users_userId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, userId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        Collection<Task> collection = findAll(projectId, userId);
        @NotNull final String removeQuery = "DELETE FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, userId);
        removePreparedStatement.setString(2, projectId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @Override
    public boolean contains(@NotNull final String entityId) throws SQLException {
        @Nullable final Task task = findOne(entityId);
        if (task != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Task> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    public boolean contains(
            @NotNull final String taskName,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE users_userId = ? " +
                "AND taskName = ? AND projects_projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, taskName);
        preparedStatement.setString(3, null);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final Task task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (task != null) return true;
        return false;
    }

    public boolean contains(
            @NotNull final String projectId,
            @NotNull final String taskName,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks WHERE users_userId = ? " +
                "AND projects_projectName = ? AND taskName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, taskName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable final Task task = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (task != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? AND " +
                "taskName LIKE '%'?'%' AND taskDescription LIKE '%'?'%';";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, pattern);
        preparedStatement.setString(4, pattern);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? AND " +
                "taskName LIKE '%'?'%' AND taskDescription LIKE '%'?'%';";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, null);
        preparedStatement.setString(3, pattern);
        preparedStatement.setString(4, pattern);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull final String pattern
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE taskName LIKE '%'?'%' AND taskDescription LIKE '%'?'%';";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, pattern);
        preparedStatement.setString(2, pattern);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY taskStartDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY projectStartDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, null);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate() throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "ORDER BY taskStartDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY taskFinishDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY taskFinishDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, null);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate() throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "ORDER BY taskFinishDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStatus(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "WHERE users_userId = ? AND projects_projectId = ? " +
                "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, null);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStatus() throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.tasks " +
                "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

}